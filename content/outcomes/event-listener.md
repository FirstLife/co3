---
title: "Events Listener"
date: 2020-06-27T19:51:23+01:00
draft: false
---

The listener is a server-side application using Web3.js in order to connect to the blockchain endpoint and capture the events generated when the smart contracts execute. It translates the events in the format needed by the wallet application and OnToMap application. The technologies used by the listener are:
-  Typescript
-  Express.js
-  Node.js
-  Mongoose
-  Web3.js
-  Truffle


Source code repository:
[https://gitlab.di.unito.it/co3-project/events-listener](https://gitlab.di.unito.it/co3-project/events-listener)