---
title: "Areaviewer"
date: 2020-06-26T19:52:23+01:00
draft: false
---

The AreaViewer is a web map based application providing a view of Co3Project aggregated data based on OnToMap. It works in combination with the InputMap component, exploiting the explicit relations between application data of Co3Project components and OnToMap entities. The AreaViewer is a component of the LandingPage that can be included in any Co3Project component trough iFrame (embed) and be controlled via url. The purposes of the AreaViewer are enhancing the overall look and feel of Co3Project platform and providing a coherent visualisation of the current status of Co3Project instances across components. It is web map based on Leaflet 1x. and Angular 2.x.

##### Source code  repository link: 
[https://gitlab.di.unito.it/co3-project/areaviewer](https://gitlab.di.unito.it/co3-project/areaviewer)
