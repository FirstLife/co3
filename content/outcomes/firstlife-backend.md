---
title: "Firstlife Backend"
date: 2020-06-26T19:51:23+01:00
draft: false
---

The backend of FirstLife is developed using LoopBack, a NodeJS based framework that provides a model-oriented approach in which the definition of a high-level data model allows the framework to offers to the user an abstraction layer between the business logic
and database technologies. In this manner, Loopback can supports in a seamless way, different storage technologies, like relational databases, NoSQL, in-memory, etc.

A customizable REST layer allows the user to access, create and modify entities of the model. Currently, FirstLife uses PostgreSQL as storage engine and the pgLatLon for geographical support (see Section 7.1).

The backend can be invoked by the client or third-part applications via API REST. FirstLife APIs uses a JavaScript Object Notation JSON as message format, in particular an extension of JSON meant for geographical entities: GeoJSON . GeoJSON is a standard format for geographical data, supported by all major GIS (geographical information system) and web GIS software.

The API layer is supported by an access control layer ACL defining users’ permissions over contents. The user authentication within the Co3Project project is provided by the afore mentioned Unified Co3Project User Management system.
The same module is also responsible for authorization tasks, implementing the well-known OAuth 2.0 Authorization Code flow pattern with the role of “Resource Server”. Also in this case, the integration with Co3Project is ensured by the use of the UWUM framework.

##### Source code  repository link: 
[https://gitlab.di.unito.it/co3-project/backend](https://gitlab.di.unito.it/co3-project/backend)
