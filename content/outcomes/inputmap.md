---
title: "Inputmap"
date: 2020-06-26T19:51:23+01:00
draft: false
---

The InputMap provides a unified system for spatial input in Co3Project. It is meant to enhance the overall look and feel of the Co3Project platform, providing an alternative to the different input methods used within Co3Project components. Moreover, the InputMap, working in combination with OnToMap, has the role to consolidate the data within Co3Project, providing a mechanism to create explicit connections between components’ data and entities in OnToMap.
The InputMap is web map based on Leaflet >= 1 and Angular >= 2 to collect enhanced spatial information as an input on a map. The map output is a triple (x, y, z) equivalent to latitude, longitude and map zoom level. Moreover, the output indicates the ID and URI of the relevant geographical entity if available.


##### Source code  repository link: 
[https://gitlab.di.unito.it/co3-project/inputmap](https://gitlab.di.unito.it/co3-project/inputmap)
