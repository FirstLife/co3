---
title: "Firstlife Tile Sever"
date: 2020-06-26T19:51:23+01:00
draft: false
---

Sources of cartographic information are indexed as tiles (x, y, z) with the format of SVG, GeoJSON or image (PNG, JPEG). The source can be locally stored or remotely retrieved (handled by the local storage).

##### Source code  repository link: 
[https://gitlab.di.unito.it/co3-project/tileserver](https://gitlab.di.unito.it/co3-project/tileserver)
