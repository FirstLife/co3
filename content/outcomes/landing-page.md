---
title: "Landing Page"
date: 2020-06-26T19:51:23+01:00
draft: false
---

The Co3 Landing Page is a Co3Project platform component which displays data about user activities that are collected by the other front-end applications of the platform; data is sent to the OTM LOGGER by the front-end applications themselves and it is showed on the Co3 Landing Page .
The Co3 Landing Page displays exclusively public, geographic data; it doesn’t verify neither the quality nor the appropriateness of the data sent to it, since this task is demanded to the other front-end applications originally collecting the data.
The Co3 Landing Page doesn’t have access to any personal data belonging to the users, after the user logged in Co3 Landing Page receives numeric user id (UWUM ID ) and username , data is used to display username on the navigation bar and to associate the logged events to the users generating them through their actions (e.g., the editing of a geographic piece of data).

##### Source code  repository link: 
[https://gitlab.di.unito.it/co3-project/landing-page](https://gitlab.di.unito.it/co3-project/landing-page)
